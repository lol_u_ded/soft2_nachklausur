package g.bigcl;

import g.bigcl.parts.*;

public class CarFactory extends Car {

    public CarFactory(Engine engine, Wheel wheels, Chassis chassis) {
        super(engine, wheels, chassis);
    }

    public Car createPremiumClass() {
        return new Car(EngineFactory.createPremiumClass(), WheelFactory.createPremiumClass(), ChassisFactory.createPremiumClass()) {
        };
    }

    public Car createCustomerClass() {
        return new Car(EngineFactory.createCustomerClass(), WheelFactory.createCustomerClass(), ChassisFactory.createCustomerClass()) {
        };
    }

    public Car createBudgetClass() {
        return new Car(EngineFactory.createBudgetClass(), WheelFactory.createBudgetClass(), ChassisFactory.createBudgetClass()) {
        };
    }

    public Car createChinaClass() {
        return new Car(EngineFactory.createChinaClass(), WheelFactory.createChinaClass(), ChassisFactory.createChinaClass()) {
        };
    }

}
