package g.bigcl.vis;

import g.bigcl.parts.Car;
import g.bigcl.parts.Chassis;
import g.bigcl.parts.Engine;
import g.bigcl.parts.Wheel;

public interface Visitor {
    void visitWheel(Wheel wheel);

    void visitChassis(Chassis chassis);

    void visitEngine(Engine engine);

    void visitCar(Car car);
}
