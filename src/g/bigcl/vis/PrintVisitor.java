package g.bigcl.vis;

import g.bigcl.parts.Car;
import g.bigcl.parts.Chassis;
import g.bigcl.parts.Engine;
import g.bigcl.parts.Wheel;

public class PrintVisitor implements Visitor {

    @Override
    public void visitWheel(Wheel wheel) {

    }

    @Override
    public void visitChassis(Chassis chassis) {

    }

    @Override
    public void visitEngine(Engine engine) {

    }

    @Override
    public void visitCar(Car car) {

    }
}
