package g.bigcl.parts;

public enum Quality {
    PREMIUM(100), GOOD(75), ACCEPTABLE(50), CHEAP(25);

    private int durability;

    Quality(int durability) {
        this.durability = durability;
    }

    public int getDurability() {
        return durability;
    }

    public void setDurability(int add) {
        this.durability += add;
    }
}
