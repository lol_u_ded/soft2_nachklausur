package g.bigcl.parts;

import g.bigcl.vis.Visitor;

public abstract class Engine {

    private final int kW;
    private final int ccm;
    private final Quality quality;
    private final Fuel fuel;
    private final double price;

    public Engine(int kW, int ccm, Quality quality, Fuel fuel, double price) {
        this.kW = kW;
        this.ccm = ccm;
        this.quality = quality;
        this.fuel = fuel;
        this.price = price;
    }

    public int getkW() {
        return kW;
    }

    public int getCcm() {
        return ccm;
    }

    public Quality getQuality() {
        return quality;
    }

    public Fuel getFuel() {
        return fuel;
    }

    public double getPrice() {
        return price;
    }

    public void accept(Visitor visitor) {
        visitor.visitEngine(this);
    }

    public enum Fuel {
        DIESEL, GASOLINE, ELECTRICITY, NATURAL_GAS;
    }
}
