package g.bigcl.parts;

import g.bigcl.vis.Visitor;

public abstract class Wheel {

    private final double price;
    private final String name;
    private final Quality quality;

    public Wheel(double price, String name, Quality quality) {
        this.price = price;
        this.name = name;
        this.quality = quality;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public Quality getQuality() {
        return quality;
    }

    public void accept(Visitor visitor) {
        visitor.visitWheel(this);
    }


}
