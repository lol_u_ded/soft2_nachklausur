package g.bigcl.parts;

import g.bigcl.vis.Visitor;

import java.awt.*;

public abstract class Chassis {

    private final Color color;
    private final Quality quality;
    private final double price;

    public Chassis(Color color, Quality quality, double price) {
        this.color = color;
        this.quality = quality;
        this.price = price;
    }

    public Color getColor() {
        return color;
    }

    public Quality getQuality() {
        return quality;
    }

    public double getPrice() {
        return price;
    }

    public void accept(Visitor visitor) {
        visitor.visitChassis(this);
    }
}
