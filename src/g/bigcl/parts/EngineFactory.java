package g.bigcl.parts;

public class EngineFactory extends Engine {


    EngineFactory(int kW, int ccm, Quality quality, Fuel fuel, double price) {
        super(kW, ccm, quality, fuel, price);
    }

    public static Engine createPremiumClass() {
        return new Engine(221, 6000, Quality.PREMIUM, Fuel.GASOLINE, 25000) {
        };
    }

    public static Engine createCustomerClass() {
        return new Engine(147, 3000, Quality.ACCEPTABLE, Fuel.ELECTRICITY, 15000) {
        };
    }

    public static Engine createBudgetClass() {
        return new Engine(110, 2500, Quality.GOOD, Fuel.GASOLINE, 10000) {
        };
    }

    public static Engine createChinaClass() {
        return new Engine(55, 1000, Quality.CHEAP, Fuel.DIESEL, 5000) {
        };
    }
}
