package g.bigcl.parts;

import g.bigcl.vis.Visitor;

public abstract class Car {

    private final Engine engine;
    private final Wheel wheels;
    private final Chassis chassis;
    private final int NUMBER_OF_WHEELS = 4;
    private double price;

    public Car(Engine engine, Wheel wheels, Chassis chassis) {
        this.engine = engine;
        this.wheels = wheels;
        this.chassis = chassis;
        calculatePrize();
    }

    private void calculatePrize() {
        price = wheels.getPrice() * NUMBER_OF_WHEELS +
                chassis.getPrice() +
                engine.getPrice();
    }

    public Engine getEngine() {
        return engine;
    }

    public Wheel getWheels() {
        return wheels;
    }

    public Chassis getChassis() {
        return chassis;
    }

    public int getNUMBER_OF_WHEELS() {
        return NUMBER_OF_WHEELS;
    }

    public double getPrice() {
        return price;
    }

    public void accept(Visitor visitor) {
        visitor.visitCar(this);
    }
}
