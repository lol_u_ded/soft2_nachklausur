package g.bigcl.parts;

public class WheelFactory extends Wheel {

    private WheelFactory(double price, String name, Quality quality) {
        super(price, name, quality);
    }

    public static Wheel createPremiumClass() {
        return new Wheel(589.69, "Micheline Pilot Sport", Quality.PREMIUM) {
        };
    }

    public static Wheel createCustomerClass() {
        return new Wheel(121.30, "Goodyear Vector 4 Seasons G3", Quality.GOOD) {
        };
    }

    public static Wheel createBudgetClass() {
        return new Wheel(76.40, "Continental AllSeasonContact", Quality.ACCEPTABLE) {
        };
    }

    public static Wheel createChinaClass() {
        return new Wheel(36.48, "Goodride", Quality.CHEAP) {
        };
    }
}
