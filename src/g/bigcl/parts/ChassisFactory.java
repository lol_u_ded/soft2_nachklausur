package g.bigcl.parts;

import java.awt.*;

public class ChassisFactory extends Chassis {
    public ChassisFactory(Color color, Quality quality, double price) {
        super(color, quality, price);
    }

    public static Chassis createPremiumClass() {
        return new Chassis(Color.RED, Quality.PREMIUM, 50000) {
        };
    }

    public static Chassis createCustomerClass() {
        return new Chassis(Color.BLUE, Quality.GOOD, 25000) {
        };
    }

    public static Chassis createBudgetClass() {
        return new Chassis(Color.BLACK, Quality.ACCEPTABLE, 12500) {
        };
    }

    public static Chassis createChinaClass() {
        return new Chassis(Color.PINK, Quality.CHEAP, 4125) {
        };
    }
}
