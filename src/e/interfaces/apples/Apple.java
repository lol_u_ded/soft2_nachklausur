package e.interfaces.apples;

import java.awt.*;

public class Apple implements Comparable<Apple> {

    private final String variety;
    private final Color color;
    private final int weight;

    public Apple(String variety, Color color, int weight) {
        this.variety = variety;
        this.color = color;
        this.weight = weight;
    }


    // Compares Apples to variety, color, weight
    @Override
    public int compareTo(Apple other) {
        int result = this.variety.compareTo(other.variety);
        if (result == 0) {
            result = Integer.compare(this.color.getRGB(), other.color.getRGB());
            if (result == 0) {
                result = Integer.compare(this.weight, other.weight);
            }
        }
        return result;
    }

}
