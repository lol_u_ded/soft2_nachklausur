package a.generics.hr;

import java.util.ArrayList;
import java.util.List;

public class DemoHR {

    public static void main(String[] args) {

        Object object = new Object();
        String string = "Hello";
        object = string;                // allowed

        Employee emp = new Employee();
        Accountant acc = new Accountant();
        emp = acc;                      // allowed

        ArrayList<Employee> empList = new ArrayList<>();
        ArrayList<Employee> empList2 = new ArrayList<>();
        ArrayList<Accountant> accList = new ArrayList<>();
//        empList = accList             // not allowed
        // Java doesnt know anything about accList, Vererbung geht hier nicht;
        ArrayList<?> varList = new ArrayList<>();
        varList = accList;              // <?> can take any Type, but one casted its fix again..
//        varList.add(emp);
        accList.add(acc);
//        varList.add(acc);             // not allowed

        //Upper Bound
        ArrayList<? extends Employee> upList = new ArrayList<>();
        upList = new ArrayList<Accountant>();
//        upList = new ArrayList<HRManager>();          // HRManager does not extend Employee -> no Child
//        upList = new ArrayList<Object>();             // Same. It has zu be at least Employee or ChildClass


        // Lower Bound
        ArrayList<? super Employee> downList = new ArrayList<>();
        downList = new ArrayList<Object>();
//        downList = new ArrayList<Accountant>();       // Arraylist extends Employee, but only Employee or it`s Super
//        downList = new ArrayList<HRManager>();        // -Classes are allowed.  Same for HRManager again.
        // no superclass of Employee


        empList.add(new Employee());
        empList.add(new Employee());
        empList.add(new Employee());
        empList2.add(new Employee());
        empList.add(new Employee());
        empList.add(new Employee());
        upList = empList;
        downList = empList;

        downList.add(new Accountant());               // allowed to add another Employee or Accountant
//        upList.add(new Employee());                 // Not allowed to add another Object

        upList.remove(1);                       // removing is perfectly fine
        downList.remove(1);

//        emp = downList.get(0);                      // not reading operation allowed
        emp = upList.get(0);
        emp.work();


    }

    public static void makeEmWork(List<? extends Employee> employees) {
        for (Employee emp : employees) {
            emp.work();
        }
    }
}
