package b.lambdas.a;


import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsFx {
    public static void main (String[] args) {
        System.out.println();

        List<String> words = List.of("Lambda", "expressions", "are", "great", "!");


        // Functions Input --> Output
        List<Integer> lengthsWorks = words.stream().map(s -> s.length()).collect(Collectors.toList());
        System.out.println("Lengths of words: ");
        lengthsWorks.forEach(s -> System.out.print(s + " "));
        System.out.println("\n");


        // Predicate Input --> Boolean
        List<String> longWords = words.stream().filter(s -> s.length() > 3).collect(Collectors.toList());
        System.out.println("Long words : ");
        longWords.forEach(s -> System.out.print(s + " "));
        System.out.println("\n");


        //Consumer Input --> no Output (eg. forEach / collect)
        System.out.println("Demonstrate consumer : ");
        longWords.forEach(s -> System.out.print("nomnomnom! "));
        System.out.println("\n");


        // Supplier no Input --> Output
        List<Double> randoms = Stream.generate(() -> Math.random())
                .limit(3)
                .collect(Collectors.toList());
        System.out.println("Random numbers : ");
        randoms.forEach(r -> System.out.print(r + " "));
        System.out.println("\n");


        // BiFunction
        int totalLength = words.stream().map(s -> s.length()).reduce(0, (w, tl) -> w + tl);
        System.out.println("Total length : " + totalLength + "\n");

        String sentence = words.stream().reduce("", (s, w) -> s + w + " ");
        System.out.println("As complete sentence: ");
        System.out.println(sentence);

    }

//    public static <T> List<T> generate(int n, Supplier<T> supplier)       // seriously I have no Idea what he was smokin' while writing that script...

}
