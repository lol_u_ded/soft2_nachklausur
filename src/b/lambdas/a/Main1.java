package b.lambdas.a;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;


public class Main1 {

    public static <T> Function twice(Function<T,T> fn){
        return new Function() {
            @Override
            public Object apply(Object o) {
                return fn.apply(fn.apply((T) o));
            }
        };
    }

    public static void main (String[] args){

        // Funktion als anonyme Klasse

        Function<Integer,Integer> square = new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer integer) {
                return integer * integer;
            }
        };



        List<Integer> ints = List.of(1,2,3,4,5,6);
//        List<Integer> squared = map(ints, twice(square));        /*        Skriptum ? u okay? */

    }

}
