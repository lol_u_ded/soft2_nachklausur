package b.lambdas.a;

import java.util.HashMap;
import java.util.Map;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;

public class Funktionale {

    public static void main(String[] args){

        // Create Testing Lists
        List<String> list = Arrays.asList("Rubi", "Mark", "Ralf", "Eva","Jesus","Randy", "rRollo");
        List<String> words = List.of("Lambda", "expressions", "are", "great", "!");

        // Predicate
        Predicate<String> r = s -> s.toLowerCase().startsWith("r");

        System.out.println("\nPredicate (Starts with \"R\"): ");
        for(String str : list){
            if(r.test(str)){
                System.out.println(str);
            }
        }
        System.out.println();


        // Binary Operator


    }
}
