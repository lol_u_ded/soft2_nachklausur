package b.lambdas.b;

import java.math.BigInteger;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static b.lambdas.b.StreamExercise.*;

public class AufgabeTwo {

    public static void main(String[] args){
        System.out.println("\n");
        System.out.println("Testen der Methoden\n");
        Beverage beverages[] = {
                new Beverage("InLänder", Package.BOTTLE, 1000, 38),
                new Beverage("RedBull", Package.CAN, 250,0),
                new Beverage("Zipfer Urtyp", Package.BOTTLE, 330,3.8),
                new Beverage("Eristof Ice", Package.BOTTLE, 250, 6.4),
                new Beverage("TrinkKakao", Package.TETRAPAK, 750,0),
                new Beverage("CapriSonne", Package.OTHER, 195,0),
                new Beverage("Stiegl Gold", Package.CAN, 330,5.9),
                new Beverage("Gösser Märzen", Package.BOTTLE, 330,3.4),
                new Beverage("Wieslburger", Package.CAN, 330,4.8),
                new Beverage("Guinnes", Package.BOTTLE, 330,3.8)};

        List<Beverage> tetra = getBeverageWithPackaging(beverages, Package.TETRAPAK);
        System.out.println("getBeverageWithPackaging(TETRA)");
        for( Beverage b : tetra){
            System.out.println(b);
        }
        System.out.println();

        List<Beverage> bottled = getBottledBeverages(beverages);
        System.out.println("getBottledBeverages");
        for( Beverage b : bottled){
            System.out.println(b);
        }
        System.out.println();

        System.out.println("getTotalVolume (in ml) ");
        System.out.println("Tetra: " + getTotalVolume(tetra));
        System.out.println("Bottles: " + getTotalVolume(bottled));
        System.out.println("All: " + getTotalVolume(List.of(beverages)) +"\n");

        System.out.println("getAverageAlcoholPercentOfAlcoholicBeverages");
        System.out.println("Bottles vol.% : " + getAverageAlcoholPercentOfAlcoholicBeverages(bottled) + "\n");

        System.out.println("getAverageAlcoholAmountOfAlcoholicBeverages");
        System.out.println("Bottles ~ml/drink : " + getAverageAlcoholAmountOfAlcoholicBeverages(bottled) + "\n");


        System.out.println("printCannedAlcoholicDrinksSortedByValue");
        printCannedAlcoholicDrinksSortedByValue(List.of(beverages));

        System.out.println("\ncontainsBeverage");
        System.out.println("Soda    --> " + containsBeverage(beverages, "Soda") + " / expect false");
        System.out.println("RedBull --> " + containsBeverage(beverages, "RedBull") + " / expect true");

    }
}

class StreamExercise{

    //Random Generator
    private static Random RANDOM_GENERATOR = new Random();
    private static final String[] EU_COUNTRIES = new String[] {"Austria","Germany","France","Belgium","Netherlands","Hungary","Croatia","Italy"};

    public static List<Beverage> getBeverageWithPackaging(Beverage[] beverages, Package packaging){
        return Stream.of(beverages)
                .filter(s -> s.getPackaging().equals(packaging))
                .collect(Collectors.toList());
    }

    public static List<Beverage> getBottledBeverages(Beverage[] beverages){
        return getBeverageWithPackaging(beverages, Package.BOTTLE);
    }

    public static double getTotalVolume(List<Beverage> beverages){
        return beverages.stream()
                .map( b -> b.getVolume())
                .reduce(0.0 , (b,v) -> b + v);
    }

    public static double getAverageAlcoholAmountOfAlcoholicBeverages(List <Beverage> beverages)throws NoSuchElementException{

        return beverages
                .stream()
                .filter(b -> b.containsAlcohol())
                .mapToDouble(b -> b.calculateAlcoholAmount())
                .average()
                .orElseThrow(()-> new NoSuchElementException());
    }
    public static double getAverageAlcoholPercentOfAlcoholicBeverages(List <Beverage> beverages)throws NoSuchElementException{

        return beverages
                .stream()
                .filter(b -> b.containsAlcohol())
                .mapToDouble(b -> b.getAlcoholPercent())
                .average()
                .orElseThrow(()-> new NoSuchElementException());
    }

    public static void printCannedAlcoholicDrinksSortedByValue(List<Beverage> beverages){
        beverages.stream()
                .filter(b -> b.getPackaging() == Package.CAN && b.containsAlcohol())
                .sorted((b1, b2) -> b1.getVolume().compareTo(b2.getVolume()))
                .forEach(b -> System.out.println(b));
    }

    public static boolean containsBeverage (Beverage[] beverages, String name){
        return Stream.of(beverages)
                .filter(b -> b.getName().equals(name))
                .findAny().isPresent();

//        return Stream.of(beverages)
//                 .anyMatch(b -> b.getName().equals(name));
    }

    public static Stream<BigInteger> getPowerOfTwoStream(){
        return Stream.iterate(0, n -> n+1).map(n -> BigInteger.TWO.pow(n));
    }

    public static List<String> getDistinctRandomCountries(int limit){
        return Stream.generate(() -> EU_COUNTRIES[RANDOM_GENERATOR.nextInt(EU_COUNTRIES.length -1)])
                .distinct()
                .limit(limit)
                .collect(Collectors.toList());
    }
}


class Beverage{

    private final String name;
    private final Package packaging;
    private final double volume;
    private final double alcoholPercent;

    public Beverage(String name, Package packaging,double volume, double alcoholPercent) {
        this.name = name;
        this.packaging = packaging;
        this.volume = volume;
        this.alcoholPercent = alcoholPercent;
    }

    public String getName() {
        return name;
    }

    public Package getPackaging() {
        return packaging;
    }

    public Double getVolume() {
        return volume;
    }

    public double getAlcoholPercent(){
        return alcoholPercent;
    }

    public boolean containsAlcohol(){
        if(alcoholPercent > 0.0){
            return true;
        }
        return false;
    }

    public double calculateAlcoholAmount(){
        return volume*(alcoholPercent/100);
    }

    @Override
    public String toString() {
        return name + " in " + packaging + " Volume: " + volume + " (" + alcoholPercent + " %)";
    }
}

 enum Package {
     BOTTLE, TETRAPAK, CAN, OTHER;
}


