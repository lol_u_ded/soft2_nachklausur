package b.lambdas.b;

import java.util.List;
import java.util.Random;
import java.util.function.*;

public class AufgabeOne {

    public static void main (String[] args){

        //Bsp1  Function
        Function<String,Double> stringDoubleFunction = str -> Double.valueOf(str);
        System.out.println(stringDoubleFunction.apply("2.5") + 1.0);

        //Bsp2 Unary Function
        UnaryOperator<Double> fx = x -> x*x*x + 2*x;
        System.out.println(" f(2) = x^3 + 2x \n f(2) = " + fx.apply(2.0));

        //Bsp3 Consumer
        Consumer<Integer> loveJava = i -> System.out.println("Ich mag Java! ".repeat(i));
        loveJava.accept(3);

        //Bsp4 Function
        Function<Integer,Integer> randomEven = n ->  new Random().nextInt((n+1)/2)*2;
        System.out.println(randomEven.apply(9));

        //Bsp5 Supplier
        List<String> someEU = List.of("Austria","Germany","France","Belgium","Netherlands","Hungary","Croatia","Italy");
        Supplier<String> eu = () -> someEU.get(new Random().nextInt(someEU.size()));
        System.out.println(eu.get());

        //Bsp6 Predicate
        Predicate<Integer> isSquared = nr -> (int) Math.sqrt(nr) * (int) Math.sqrt(nr) == nr;
        System.out.println(isSquared.test(4));
        System.out.println(isSquared.test(25));
        System.out.println(isSquared.test(5));








    }
}
