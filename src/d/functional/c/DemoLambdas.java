package d.functional.c;

import d.functional.c.uebung.Person;
import klausur.d.Streams;
import org.w3c.dom.ls.LSOutput;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DemoLambdas {
    public static void main(String[] args) {

        System.out.println("Testing int[] arr to Stream: ");
        int[] arr = {1, 2, 3, 4, 5};
        Arrays.stream(arr)
                .filter(x -> x % 2 == 0)
                .forEach(x -> System.out.print(x + " "));
        System.out.println("\n");

        List<Person> people = Arrays.asList(
                new Person("Charles", "Dickens", 60),
                new Person("Lewis", "Carroll", 42),
                new Person("Thomas", "Charlyle", 51),
                new Person("Charlotte", "Bronte", 45),
                new Person("Matthew", "Arnold", 39),
                new Person("Sally", "Emmer", 30),
                new Person("Hugo Florian", "Braun", 47),
                new Person("Carina", "Grill", 26),
                new Person("Adalbert Ferdinand", "Schulz", 47),
                new Person("Eugen", "Freud", 99),
                new Person("Naho", "Ching", 28),
                new Person("Bagundo", "Uga", 49),
                new Person("Ash", "Ketchum", 11),
                new Person("Zott", "Monte", 32),
                new Person("Leopold", "Knödel", 72)
        );

        // ue1 : Sort list by last name
//        people.sort(Comparator.comparing(Person::getLastName));

//        people.sort((s,t ) -> s.getLastName().compareTo(t.getLastName()));

        people = people.stream()
                .sorted((x, y) -> (x.getLastName().compareTo(y.getLastName())))
//                .forEach(x -> System.out.println(x));
//                  OR if you want a List
                .collect(Collectors.toList());


        // ue2 : Create a Method that prints all elements of the List
        System.out.println("\nPrint out previous sorted List (by Las name) ");
        people.forEach(s -> System.out.println(s));

        // ue3 : Create a Method that prints all people that have last name beginning with "C"
        System.out.println("\nPrint all where last name begins with \"C\"");
        people.stream().filter(s -> s.getLastName().startsWith("C")).forEach(System.out::println);

        // use printConditionally
        System.out.println("\nPrint all that are older 44");
        printConditionally(people, a -> a.getAge() >= 45);

        // use performConditionally
        System.out.println("\nUser can define What to do with Persons machtching Predicate");
        performConditionally(people, a -> a.getAge() >= 45, b -> System.out.print(b.getFirstName() + " YAY! "));
        System.out.println("\n");


        // use BiConsumer Method
        int[] numbers = {1, 2, 3, 4, 5};
        int key = 2;
        System.out.println("\nUser can define a BiConsumer:");
        process(numbers, key, (x, y) -> System.out.println(x + y));
        System.out.println("\n");

        // ForEach with IfElse Condition
        System.out.println("\nForEach with ifElse: ");
        people.forEach(p -> {
            if (p.getAge() <= 45) {
                System.out.println(p);
            } else {
                System.out.println(p.getFirstName() + " is way to old");
            }
        });
        System.out.println("\n");

        // Create a Stream snake (lol)
        System.out.println("\nlittle Stream:  ");
        people
                .stream()
                .filter(x -> x.getAge() >= 45)
                .limit(5)
                .sorted((a, b) -> Integer.compare(a.getAge(), b.getAge()))
                .forEach(p -> System.out.println(p));
        System.out.println("\n");

        // use the count() after filtering
        System.out.println("\nCount words starting with 'C' Stream ");
        long count = people
                .stream()
                .filter(a -> a.getFirstName().startsWith("C"))
                .count();
        System.out.println(count);
        System.out.println("\n");


        // reduce

        // Stream of persons --map--> Stream of Integer . int

        System.out.println("Sum of the 3 youngest persons");
        int youngestSum = people
                .stream()
                .map(x -> x.getAge())                            // .map(Person::getAge)
                .sorted((x, y) -> Integer.compare(x, y))          //.sorted(Integer::compare)
                .limit(3)
                .reduce(0, (x, y) -> x + y);             //  .reduce(0, Integer::sum);

        System.out.println(" ySum = " + youngestSum);
        System.out.println("\n");

        System.out.println(" Age of 3 youngest persons");
        people
                .stream()
                .map(x -> x.getAge())                            // .map(Person::getAge)
                .sorted((x, y) -> Integer.compare(x, y))          //.sorted(Integer::compare)
                .limit(3)
                .forEach(x -> System.out.println(x));

        System.out.println("\n");


        System.out.println(" 3 youngest persons");
        List<Person> ppl2 = people
                .stream()
                .sorted((x, y) -> Integer.compare(x.getAge(), y.getAge()))          //.sorted(Integer::compare)
                .limit(3)
                .collect(Collectors.toList());

        ppl2.forEach(x -> System.out.println(x));                   //ppl2.forEach(System.out::println);
        System.out.println();


        System.out.println("Average Age");
        Double avgAge = people
                .stream()
                .map(x -> x.getAge())
                .mapToInt(x -> x)
                .average()
                .getAsDouble();
        System.out.println(" avg = " + avgAge.intValue());
        System.out.println("\n");


        System.out.println("Statistics");
        IntSummaryStatistics stats = people
                .stream()
                .map(x -> x.getAge())
                .mapToInt(x -> x)
                .summaryStatistics();
        System.out.println(" Stats: " + stats);
        System.out.println("\n");


        System.out.println("Find Max with Reduce");
        Optional<Integer> max = people
                .stream()
                .map(x -> x.getAge())
                .reduce((currMax, x) -> {
                    if (currMax < x) {
                        currMax = x;
                    }
                    return currMax;
                });
//        if(max.isPresent()) {
//            System.out.println("MaxAge: " + max.get());
//        }else{
//            System.out.println("Error lol");
//        }
        max.ifPresent(x -> System.out.println("MaxAge: " + x));
        System.out.println("\n");


        System.out.println("Longest Name");
        String longest = people
                .stream()
                .max(Comparator.comparingInt(a -> a.getFirstName().length()))
                .toString();
        System.out.println(longest + " <-- is longest\n");

        System.out.println("Grouping By (older/younger 45) ");
        Map<Boolean, List<Person>> grouping =
                people
                        .stream()
                        .collect(Collectors.groupingBy(p -> p.getAge() > 45));

        grouping.forEach((k, v) -> System.out.println("Key: " + k + ", Values:\n  " + v + "\n"));


        System.out.println("Personen (last3) absteigend v.1");
        people
                .stream()
                .sorted((x, y) -> y.getLastName().compareTo(x.getLastName()))
                .distinct()
                .limit(3)
                .forEach(p -> System.out.println(p));
        System.out.println("\n");


        System.out.println("Personen (last3) absteigend v.2");
        people
                .stream()
                .sorted(Comparator.comparing(Person::getLastName).reversed())
                .limit(3)
                .forEach(System.out::println);
        System.out.println("\n");


    }
    // Prints Person with given Conditions
    private static void printConditionally(List<Person> ppl, Predicate<Person> predicate) {
//        for (Person p : ppl) {
//            if (predicate.test(p)) {
//                System.out.println(p);
//            }
//        }
        ppl.stream()
                .filter(predicate)
                .forEach(x -> System.out.println(x));
    }

    // User define What to do with Persons matching given Conditions
    private static void performConditionally(List<Person> ppl, Predicate<Person> testIt, Consumer<Person> nomNomNom) {
//        for (Person p : ppl) {
//            if (testIt.test(p)) {
//                nomNomNom.accept(p);
//            }
//        }
        ppl.stream().filter(testIt).forEach(nomNomNom);
    }

    // User can define a Bi-Consumer
    public static void process(int[] arr, int key, BiConsumer<Integer, Integer> biConsumer) {
        for (Integer i : arr) {
            biConsumer.accept(i, key);
        }
    }

}
