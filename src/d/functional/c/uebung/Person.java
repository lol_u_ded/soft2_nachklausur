package d.functional.c.uebung;

public class Person implements Comparable<Person> {

    private final String firstName;
    private final String lastName;
    private final int age;

    public Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return lastName + " " + firstName + " " + age;
    }

    @Override
    public int compareTo(Person other) {
        int comp = this.lastName.compareTo(other.getLastName());
        if (comp == 0) {
            comp = this.firstName.compareTo(other.firstName);
            if (comp == 0) {
                comp = Integer.compare(this.getAge(), other.getAge());
            }
        }
        return comp;
    }
}
