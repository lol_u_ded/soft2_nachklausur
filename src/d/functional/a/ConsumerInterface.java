package d.functional.a;

import java.util.List;
import java.util.function.Consumer;

public class ConsumerInterface {

    public static void main(String[] args) {
        List<Integer> numList = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Consumer consumer1 = new Consumer<Integer>() {

            @Override
            public void accept(Integer i) {
                System.out.print(" " + i);
            }
        };

        System.out.println("Consumer implement full Interface");
        numList.forEach(consumer1);


        System.out.println("\nConsumer Lambda");
        numList.forEach(s -> System.out.print(" " + s.compareTo(5)));
        System.out.println("\n");


    }
}
