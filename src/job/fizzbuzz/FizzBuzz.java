package job.fizzbuzz;

public class FizzBuzz {
    public static void main(String[] args) {
        printTo(100);
        print2To(50);

    }

    public static void printTo(int n) {
        for (int i = 1; i <= n; i++) {
            boolean print = false;
            if (i % 3 == 0) {
                print = true;
                System.out.print("Fizz");
            }
            if (i % 5 == 0) {
                print = true;
                System.out.print("Buzz");
            }
            if (!print) {
                System.out.println(i);
            } else {
                System.out.println();
            }
        }
    }

    public static void print2To(int n) {
        for (int i = 1; i <= n; i++) {
            int three = i % 3;
            int five = i % 5;
            if (three > 0 && five > 0) {
                System.out.println(i);
            } else {
                if (three == 0) {
                    System.out.print("Fizz");
                }
                if (five == 0) {
                    System.out.print("Buzz");
                }
                System.out.println();
            }
        }
    }
}
