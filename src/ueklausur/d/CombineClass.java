package ueklausur.d;


import java.util.List;

public class CombineClass {
    public static void main(String[] args) {

        BasicTask b1 = new BasicTask(5);
        CombinedTasks combinedTasks = new CombinedTasks(b1,
                new CombinedTasks(new BasicTask(5), new BasicTask(5), new BasicTask(5)));

        System.out.println(combinedTasks.getDuration());
    }
}

class BasicTask {
    private int duration;

    public BasicTask(int duration) {
        this.duration = duration;
    }

    public int getDuration() {
        return duration;
    }
}

class CombinedTasks extends BasicTask {

    List<BasicTask> listOfTasks;

    public CombinedTasks(BasicTask... tasks) {
        super(0);
        listOfTasks = List.of(tasks);
    }

    @Override
    public int getDuration() {
        return super.getDuration() +
                listOfTasks.stream()
                        .map(x -> x.getDuration())
                        .reduce(0, (p, c) -> p + c);
    }
}
