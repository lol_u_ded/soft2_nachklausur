package ueklausur.b;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Streams {
    public static void main(String[] args) {

        List<Person> personList = List.of(
                new Person("Steve", 18, Set.of("Ball-Spielen", "League of Legends zocken")),
                new Person("Robert", 21, Set.of("Programmieren", "League of Legends zocken")),
                new Person("Martha", 14, Set.of("Klettern", "Kampfsport")),
                new Person("Eugen", 25, Set.of("League of Legends zocken", "Pokemon Go")),
                new Person("Lisa", 17, Set.of("Laufen", "Saufen")),
                new Person("Carol", 20, Set.of("League of Legends zocken", "Ausgehen"))
        );

        // alle Personen 18+

        long grownUp =
                personList
                        .stream()
                        .filter(x -> x.getAge() >= 18)
                        .count();

        System.out.println("Über 18 : " + grownUp + "\n");

        //absteigende Liste

        List<String> descOrdName =
                personList
                        .stream()
                        .map(x -> x.getName())
                        .sorted((x, y) -> y.compareTo(x))
                        .collect(Collectors.toList());

        System.out.println(descOrdName.toString() + "\n");


        // flatMap same Hobbies
        long sameHobby =
                personList
                        .stream()
                        .map(person -> person.getHobbies().stream())
                        .flatMap(stream -> stream.filter(hobby -> hobby.equals("League of Legends zocken")))
                        .count();

        System.out.println("Persons with LoL as Hobby: " + sameHobby + "\n");

    }
}

class Person {

    public final String name;
    public final int age;
    public final Set<String> hobbies;


    Person(String name, int age, Set<String> hobbies) {
        this.name = name;
        this.age = age;
        this.hobbies = hobbies;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Set<String> getHobbies() {
        return hobbies;
    }
}
