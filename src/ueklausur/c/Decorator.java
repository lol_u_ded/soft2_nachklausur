package ueklausur.c;

// Interface
interface PC {
    public double getPrice();
}

public class Decorator {

    public static void main(String[] args) {

        // Basically you will return a Decorator that implements all your base classes.. the most basic is inside
        PC pc = new GPUPCDecorator(new MemoryPCDecorator(new BasicPC(), 16));

        System.out.println(pc.getPrice());
        System.out.println("250 + 16*20 + 200 = 770");
    }
}

// Basic Class undecorated this will be inherited
class BasicPC implements PC {

    @Override
    public double getPrice() {
        return 250;
    }
}

//BaseDecorator inherits BasicPC over Interface
abstract class PCDecorator implements PC {

    PC pc;

    public PCDecorator(PC pc) {
        this.pc = pc;
    }

    @Override
    public double getPrice() {
        return pc.getPrice();
    }
}

// the actual Decorators
class MemoryPCDecorator extends PCDecorator {

    private final int additionalGB;

    public MemoryPCDecorator(PC pc, int additionalGB) {
        super(pc);
        this.additionalGB = additionalGB;
    }

    @Override
    public double getPrice() {
        return super.getPrice() + additionalGB * 20.0;
    }
}

class GPUPCDecorator extends PCDecorator {

    public GPUPCDecorator(PC pc) {
        super(pc);
    }

    @Override
    public double getPrice() {
        return super.getPrice() + 200;
    }
}

