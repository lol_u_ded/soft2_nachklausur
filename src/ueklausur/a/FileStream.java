package ueklausur.a;

import java.util.Iterator;
import java.util.function.Consumer;

public class FileStream<E> implements Iterable<E> {

    public E read() {
        // ...
        return null;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {

            E next = null;

            @Override
            public boolean hasNext() {
                next = read();
                return next != null;
            }

            @Override
            public E next() {
                E result = next;
                next = null;
                return result;
            }
        };
    }

}
