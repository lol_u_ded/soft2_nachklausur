package f.allgemein.staticmeth;

public class Klasse {

    protected static void staticMethod() {
        System.out.println("Statischer Zugriff");
    }

    protected void nonStaticMethod() {
        System.out.println("Zugriff nur über Objekt");
    }
}
