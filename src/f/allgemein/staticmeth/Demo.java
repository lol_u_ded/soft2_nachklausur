package f.allgemein.staticmeth;

public class Demo {
    public static void main(String[] args) {
        Klasse.staticMethod();

        Klasse klasse = new Klasse();
        klasse.nonStaticMethod();
    }

}
