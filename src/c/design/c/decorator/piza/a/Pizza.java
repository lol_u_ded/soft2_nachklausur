package c.design.c.decorator.piza.a;

public interface Pizza {

    String getDescription();

    double getCost();

}
