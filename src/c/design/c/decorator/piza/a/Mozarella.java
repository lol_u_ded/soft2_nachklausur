package c.design.c.decorator.piza.a;

public class Mozarella extends ToppingDecorator {

    public Mozarella(Pizza pizza) {
        super(pizza);

        System.out.println("Adding Mozarella");
    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", Mozzarella";
    }

    @Override
    public double getCost() {
        return super.getCost() + 0.5;
    }


}
