package c.design.c.decorator.piza.a;

public class PlainPizza implements Pizza {

    @Override
    public String getDescription() {
        return "Pizza Dough";
    }

    @Override
    public double getCost() {
        return 4.10;
    }

}
