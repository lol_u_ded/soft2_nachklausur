package c.design.c.decorator.piza;

import c.design.c.decorator.piza.a.Mozarella;
import c.design.c.decorator.piza.a.Pizza;
import c.design.c.decorator.piza.a.PlainPizza;
import c.design.c.decorator.piza.a.TomatoSauce;

public class Demo {
    public static void main(String[] args) {
        PlainPizza pizza = new TomatoSauce(
                new Mozarella(
                        new PlainPizza()));

        System.out.println(pizza.getCost());
        System.out.println(pizza.getDescription());
    }
}
