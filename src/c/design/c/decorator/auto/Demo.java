package c.design.c.decorator.auto;

import c.design.c.decorator.auto.werkstatt.*;

public class Demo {

    public static void main(String[] args) {

        Auto auto1 = new Audi();
        Auto auto2 = new Mercedes();

        System.out.println("\nVorher");
        System.out.println("\nAudi");
        System.out.println(auto1.getAusstattung());
        System.out.println(auto1.getPrice());

        System.out.println("\nMerzedes");
        System.out.println(auto2.getAusstattung());
        System.out.println(auto2.getPrice());

        System.out.println("------------------------------------------------------");

        auto1 = new Sportgetriebe(auto1);
        auto2 = new Navi(auto2);
        auto2 = new Sportgetriebe(auto2);

        System.out.println("\nNachher");
        System.out.println("\nAudi");
        System.out.println(auto1.getAusstattung());
        System.out.println(auto1.getPrice());

        System.out.println("\nMerzedes");
        System.out.println(auto2.getAusstattung());
        System.out.println(auto2.getPrice());
    }
}
