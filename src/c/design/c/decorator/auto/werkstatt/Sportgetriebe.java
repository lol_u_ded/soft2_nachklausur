package c.design.c.decorator.auto.werkstatt;

public class Sportgetriebe extends Decorator {

    Auto auto;

    public Sportgetriebe(Auto auto) {
        this.auto = auto;
    }

    @Override
    public double getPrice() {
        return auto.getPrice() + 12000;
    }

    @Override
    public String getAusstattung() {
        return auto.getAusstattung() + " + Sportgetriebe";
    }
}
