package c.design.c.decorator.auto.werkstatt;

public class Audi extends Auto {

    public Audi() {
        ausstattung = "Audi A4";
    }

    @Override
    public double getPrice() {
        return 25000;
    }
}
