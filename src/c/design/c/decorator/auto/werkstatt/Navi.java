package c.design.c.decorator.auto.werkstatt;

public class Navi extends Decorator {

    Auto auto;

    public Navi(Auto auto) {
        this.auto = auto;
    }

    @Override
    public double getPrice() {
        return auto.getPrice() + 8000;
    }

    @Override
    public String getAusstattung() {
        return auto.getAusstattung() + " + Navigationssystem";
    }
}
