package c.design.c.decorator.auto.werkstatt;

public class Mercedes extends Auto {

    public Mercedes() {
        ausstattung = "Merzedes C3";
    }

    @Override
    public double getPrice() {
        return 30000;
    }
}
