package c.design.c.decorator.auto.werkstatt;

public abstract class Auto {

    protected String ausstattung;

    public String getAusstattung() {
        return ausstattung;
    }

    public abstract double getPrice();
}
