package c.design.c.decorator.auto.werkstatt;

public abstract class Decorator extends Auto {

    public abstract String getAusstattung();

}
