package c.design.c.decorator.cocktail;

import c.design.c.decorator.cocktail.dec.Cocktail;
import c.design.c.decorator.cocktail.dec.PlainCocktail;
import c.design.c.decorator.cocktail.fac.CocktailMixer;

public class DemoBar {
    public static void main(String[] args) {
        CocktailMixer James = new CocktailMixer();

        System.out.println("James?\n Yes Sir? \n Mix me a Mojito!\n");
        Cocktail mojito = James.getMojito();
        System.out.println(mojito.getDescription());
        System.out.println(mojito.getPrice());

        System.out.println("\n glllglllgllgggl\n James?!\n Yes sir?\n Another drink, gimme a good Gintonic!\n");
        Cocktail ginTonic = James.getClassicGinTonic();
        System.out.println(ginTonic.getDescription());
        System.out.println(ginTonic.getPrice());
    }

}

