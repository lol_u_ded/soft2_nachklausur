package c.design.c.decorator.cocktail.dec;

public class Gin extends CocktailDecorator {

    public Gin(Cocktail cocktail) {
        super(cocktail);
    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", best Gin";
    }

    @Override
    public double getPrice() {
        return super.getPrice() + 1.0;
    }
}
