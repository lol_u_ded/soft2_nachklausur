package c.design.c.decorator.cocktail.dec;

public class Tonic extends CocktailDecorator {

    public Tonic(Cocktail cocktail) {
        super(cocktail);
    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", Gentlemans Tonic";
    }

    @Override
    public double getPrice() {
        return super.getPrice() + .75;
    }
}
