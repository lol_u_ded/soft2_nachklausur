package c.design.c.decorator.cocktail.dec;

public class CocktailDecorator extends PlainCocktail {

    protected Cocktail cocktail;

    CocktailDecorator(Cocktail cocktail) {
        this.cocktail = cocktail;
    }

    public String getDescription() {
        return cocktail.getDescription();
    }

    public double getPrice() {
        return cocktail.getPrice();
    }
}
