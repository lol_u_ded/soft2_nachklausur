package c.design.c.decorator.cocktail.dec;

public class Soda extends CocktailDecorator {
    public Soda(Cocktail cocktail) {
        super(cocktail);
    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", Soda";
    }

    @Override
    public double getPrice() {
        return super.getPrice() + .1;
    }
}
