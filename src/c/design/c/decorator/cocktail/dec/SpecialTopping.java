package c.design.c.decorator.cocktail.dec;

public class SpecialTopping extends CocktailDecorator {

    private final String special;


    public SpecialTopping(Cocktail cocktail, String specialIngredient) {
        super(cocktail);
        this.special = specialIngredient;
    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", " + special;
    }

    @Override
    public double getPrice() {
        return super.getPrice() + .95;
    }
}
