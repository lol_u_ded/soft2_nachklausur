package c.design.c.decorator.cocktail.dec;

public class Rum extends CocktailDecorator {

    public Rum(Cocktail cocktail) {
        super(cocktail);
    }

    @Override
    public double getPrice() {
        return super.getPrice() + 0.35;
    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", Havanna Rum";
    }
}
