package c.design.c.decorator.cocktail.dec;

public class Sugar extends CocktailDecorator {

    public Sugar(Cocktail cocktail) {
        super(cocktail);
    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", Sugar";
    }

    @Override
    public double getPrice() {
        return super.getPrice() + 0.10;
    }
}
