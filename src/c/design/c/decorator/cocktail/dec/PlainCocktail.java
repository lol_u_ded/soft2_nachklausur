package c.design.c.decorator.cocktail.dec;

public class PlainCocktail implements Cocktail {

    private static String DEAFULT_DESCR = "Funky glass";
    private static double DEAFULT_PRICE = 4.00;


    @Override
    public String getDescription() {
        return DEAFULT_DESCR;
    }

    @Override
    public double getPrice() {
        return DEAFULT_PRICE;
    }
}
