package c.design.c.decorator.cocktail.dec;

public class Limette extends CocktailDecorator {
    public Limette(Cocktail cocktail) {
        super(cocktail);
    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", Limette";
    }

    @Override
    public double getPrice() {
        return super.getPrice() + .45;
    }
}
