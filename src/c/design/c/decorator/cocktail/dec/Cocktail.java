package c.design.c.decorator.cocktail.dec;

public interface Cocktail {

    String getDescription();

    double getPrice();

}
