package c.design.c.decorator.cocktail.fac;

import c.design.c.decorator.cocktail.dec.*;

public class CocktailMixer {

    public PlainCocktail getMojito() {
        System.out.println("I'm about to mix a fine Mojito...");
        return new SpecialTopping(
                new Soda(
                        new Limette(
                                new Sugar(
                                        new Rum(
                                                new PlainCocktail())))), "Minze and IceCubes");
    }

    public Cocktail getClassicGinTonic() {
        System.out.println("A very good decision. Gin Tonic is the best ... ");
        return new SpecialTopping(
                new Tonic(
                        new Gin(
                                new PlainCocktail())), "fresh rosemary");
    }
}
