package c.design.g.observer.formular;

import c.design.g.observer.formular.Kunde;

// angelehnt an:
// https://www.youtube.com/watch?v=JqH4KmpxbOM&ab_channel=moinLeude

public class DemoVersicherung {
    public static void main(String[] args) {

        Kunde peter = CustomerCreator.newBasicKunde("Peter", "Linz Urfahr");
        Kunde ismir = CustomerCreator.newBasicKunde("Ismir", "Wien Favoriten");
        Kunde olga = CustomerCreator.newBasicKunde("Olga", "Linz Land");

        Allianz allianz = new Allianz();

        allianz.addKunde(peter);
        allianz.addKunde(ismir);
        allianz.addKunde(olga);

        Formular formular1 = new Formular("Passierschein A38");

        allianz.setNewFormular(formular1, true);
    }
}
