package c.design.g.observer.formular;

public class SimpleVersicherung extends Versicherung {

    private final Formular vertrag;

    SimpleVersicherung(Formular formular) {
        this.vertrag = formular;
        contactClients(vertrag);
    }

    public Formular getVertrag() {
        return vertrag;
    }
}
