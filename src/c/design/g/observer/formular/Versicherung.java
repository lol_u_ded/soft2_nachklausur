package c.design.g.observer.formular;

import java.util.ArrayList;
import java.util.List;

public abstract class Versicherung {

    private final List<Kunde> kundenListe = new ArrayList<>();

    public void addKunde(Kunde kunde) {
        kundenListe.add(kunde);
    }

    public void removeKunde(Kunde kunde) {
        kundenListe.remove(kunde);
    }

    public void contactClients(Formular formular) {
        kundenListe.forEach(x -> x.formularToCustomer(formular));
    }
}
