package c.design.g.observer.formular;

public class Formular {

    private final String titel;

    public Formular(String titel) {
        this.titel = titel;
    }

    public String getTitel() {
        return titel;
    }
}
