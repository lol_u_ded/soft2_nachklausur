package c.design.g.observer.formular;

import c.design.g.observer.formular.Formular;
import c.design.g.observer.formular.Versicherung;

import java.util.ArrayList;
import java.util.List;

public class Allianz extends Versicherung {

    private final List<Formular> allContracts = new ArrayList<>();

    public void setNewFormular(Formular formular, boolean send) {
        allContracts.add(formular);
        if (send) {
            contactClients(formular);
        }
    }

    public List<Formular> getAllContracts() {
        return allContracts;
    }
}
