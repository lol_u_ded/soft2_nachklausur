package c.design.g.observer.formular;

public class CustomerCreator {

    public static Kunde newBasicKunde(String name, String city) {

        return new Kunde(name, city) {

            @Override
            void formularToCustomer(Formular formular) {
                System.out.println("Sende Formular "
                        + formular.getTitel() + " an "
                        + this.getName() + " in " + this.getCity() + ". ");
            }
        };
    }
}
