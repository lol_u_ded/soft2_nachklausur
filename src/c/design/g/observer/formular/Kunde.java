package c.design.g.observer.formular;

public abstract class Kunde {

    public final String name;
    public final String city;

    protected Kunde(String name, String city) {
        this.name = name;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    abstract void formularToCustomer(Formular formular);

}
