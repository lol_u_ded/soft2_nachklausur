package c.design.a.factory.point;

public class Demo {
    public static void main(String[] args) {

        Point cartesian = Point.newCartesianPoint(5, 5);
        System.out.println("Cartesian (x,y) = " + cartesian.getPoints());
        Point polar = Point.newPolarPoint(5, 5);
        System.out.println("Polar (x,y) = " + polar.getPoints());
    }
}
