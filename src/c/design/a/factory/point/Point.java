package c.design.a.factory.point;

public class Point {

    private final double x, y;

    // Hier wird einfach der Konstruktor versteckt
    private Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    // Static ,sodass man ohne Objekterzeugung darauf zugreifen kann
    public static Point newPolarPoint(double rho, double theta) {
        return new Point(rho * Math.cos(theta), rho * Math.sin(theta));
    }

    public static Point newCartesianPoint(double x, double y) {
        return new Point(x, y);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public String getPoints() {
        return "(" + x + " , " + y + ")";
    }
}
