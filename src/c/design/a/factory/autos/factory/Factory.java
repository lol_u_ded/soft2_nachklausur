package c.design.a.factory.autos.factory;

import c.design.a.factory.autos.factory.cars.BMW;
import c.design.a.factory.autos.factory.cars.Doge;
import c.design.a.factory.autos.factory.cars.Kawasaki;
import c.design.a.factory.autos.factory.cars.Typ;

public class Factory {

    public BMW getAudiA4() {
        return new BMW(250, 200, Typ.PKW, 300);
    }

    public Kawasaki getSpeedBike() {
        return new Kawasaki(280, 120, Typ.BIKE);
    }

    public Doge getTrucklikeCar() {
        return new Doge(165, 450, Typ.LKW, "I AM PURE POWER!");
    }
}
