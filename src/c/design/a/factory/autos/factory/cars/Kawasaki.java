package c.design.a.factory.autos.factory.cars;

public class Kawasaki extends Fahrzeug {

    public Kawasaki(int speed, int ps, Typ typ) {
        super(speed, ps, typ);
    }

    @Override
    public void getDescription() {
        super.getDescription();
        System.out.println("Kawasaki bikes are the best! ");
    }
}
