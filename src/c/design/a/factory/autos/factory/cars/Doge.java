package c.design.a.factory.autos.factory.cars;

public class Doge extends Fahrzeug {

    private final String extraString;


    public Doge(int speed, int ps, Typ typ, String extraString) {
        super(speed, ps, typ);
        this.extraString = extraString;
    }

    @Override
    public void drive() {
        super.drive();
        System.out.println("I feel " + getPs() + " PS! NICE");
        System.out.println(extraString);
    }
}
