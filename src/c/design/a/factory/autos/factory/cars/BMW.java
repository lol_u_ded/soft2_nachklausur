package c.design.a.factory.autos.factory.cars;

public class BMW extends Fahrzeug {

    private final int stylePoints;

    public BMW(int speed, int ps, Typ typ, int stylePoints) {
        super(speed, ps, typ);
        this.stylePoints = stylePoints;
    }

    @Override
    public void getDescription() {
        super.getDescription();
        System.out.println(" I have got " + stylePoints + "! ");
    }


}
