package c.design.a.factory.autos.factory.cars;

public enum Typ {

    PKW(4), LKW(6), BIKE(2);

    private final int tires;

    Typ(int i) {
        tires = i;
    }

    protected int getTires() {
        return tires;
    }

}
