package c.design.a.factory.autos.factory.cars;

public abstract class Fahrzeug {

    private final int speed;
    private final int ps;
    private final Typ typ;

    public Fahrzeug(int speed, int ps, Typ typ) {
        this.speed = speed;
        this.ps = ps;
        this.typ = typ;
    }

    public void drive() {
        System.out.println("Brooommmm");
    }

    public void getDescription() {
        System.out.println("I got " + typ.getTires() + " tires and whopping " + ps + " PS! My max Speed is " + speed + "mp/h! ");
    }

    public int getSpeed() {
        return speed;
    }

    public int getPs() {
        return ps;
    }

    public Typ getTyp() {
        return typ;
    }
}
