package c.design.a.factory.autos;

import c.design.a.factory.autos.factory.Factory;
import c.design.a.factory.autos.factory.cars.Fahrzeug;

public class DemoAuto {

    public static void main(String[] args) {

        Factory carFactory = new Factory();

        System.out.println("A4:");
        Fahrzeug audi = carFactory.getAudiA4();
        audi.getDescription();
        audi.drive();

        System.out.println("\nBike:");
        Fahrzeug bike = carFactory.getSpeedBike();
        bike.getDescription();
        bike.drive();

        System.out.println("\nPower: ");
        Fahrzeug brummi = carFactory.getTrucklikeCar();
        brummi.getDescription();
        brummi.drive();
    }


}
