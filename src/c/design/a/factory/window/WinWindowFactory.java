package c.design.a.factory.window;

public class WinWindowFactory extends WindowFactory {

    @Override
    public Window createWindow() {
        return new WinWindow();
    }
}
