package c.design.a.factory.window;

public abstract class Window {

    public abstract void open();
}
