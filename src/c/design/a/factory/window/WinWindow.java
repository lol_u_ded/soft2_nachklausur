package c.design.a.factory.window;

public class WinWindow extends Window {

    @Override
    public void open() {
        System.out.println("Opening Windows Window");
    }
}
