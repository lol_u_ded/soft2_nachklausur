package c.design.a.factory.window;

public class MacWindow extends Window {

    @Override
    public void open() {
        System.out.println("Opening Mac Window");
    }
}
