package c.design.a.factory.window;

public abstract class WindowFactory {
    public abstract Window createWindow();
}
