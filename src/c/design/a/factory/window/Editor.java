package c.design.a.factory.window;

public class Editor {
    public static void main(String[] args) {

        WindowFactory mac = new MacWindowFactory();
        WindowFactory pc = new WinWindowFactory();

        Window w = null;

        w = mac.createWindow();
        w.open();

        w = pc.createWindow();
        w.open();
    }
}
