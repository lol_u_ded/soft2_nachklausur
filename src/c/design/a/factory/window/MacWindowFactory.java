package c.design.a.factory.window;

public class MacWindowFactory extends WindowFactory {

    @Override
    public Window createWindow() {
        return new MacWindow();
    }
}
