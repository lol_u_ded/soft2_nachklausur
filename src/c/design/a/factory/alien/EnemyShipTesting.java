package c.design.a.factory.alien;

import c.design.a.factory.alien.fac.EnemyShip;
import c.design.a.factory.alien.fac.EnemyShipBuilding;
import c.design.a.factory.alien.fac.UFOEnemyShipBuilding;

public class EnemyShipTesting {

//    Way to complex, dont try to wor thruogh it .. waste of time
//    https://www.youtube.com/watch?v=xbjAsdAK4xQ&list=PLF206E906175C7E07&index=6&ab_channel=DerekBanas

    public static void main(String[] args) {

        // EnemyShipBuilding handles orders for new EnemyShips
        // You send it a code using the orderTheShip method &
        // it sends the order to the right factory for creation

        EnemyShipBuilding MakeUFOs = new UFOEnemyShipBuilding();

        EnemyShip theGrunt = MakeUFOs.orderTheShip("UFO");
        System.out.println(theGrunt + "\n");

        EnemyShip theBoss = MakeUFOs.orderTheShip("UFO BOSS");
        System.out.println(theBoss + "\n");

    }

}
