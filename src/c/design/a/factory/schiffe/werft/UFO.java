package c.design.a.factory.schiffe.werft;

public class UFO extends EnemyShip {

    protected UFO() {
        super("Strange Ship", 1000);
    }

    @Override
    public void displayEnemyShip() {
        super.displayEnemyShip();
        System.out.println("What is this strange thing!?");
    }

    @Override
    public void enemyShipShoots() {
        System.out.println("A strange red light beam is pointing in your direction");
        super.enemyShipShoots();
    }
}
