package c.design.a.factory.schiffe.werft;

public class Fregatte extends EnemyShip {

    private boolean reloading;

    protected Fregatte(String name, double amtDamage) {
        super(name, amtDamage);
        this.reloading = false;
    }

    @Override
    public void enemyShipShoots() {
        if(reloading){
            System.out.println( this.getName() + " is reloading its cannons, beware!");
            reloading = false;
        }else {
            System.out.println("What a ridiculous amount of cannons");
            super.enemyShipShoots();
            super.enemyShipShoots();
            super.enemyShipShoots();
            reloading = true;
        }
    }

    @Override
    public void displayEnemyShip() {
        super.displayEnemyShip();
        System.out.println(" This ship is one of the enemies finest..");
    }
}
