package c.design.a.factory.schiffe.werft;

public class SmallShip extends EnemyShip{

    protected SmallShip(String name, double damage) {
        super(name, damage);
    }

    @Override
    public void displayEnemyShip() {
        super.displayEnemyShip();
        System.out.println("It looks not very strong.");
    }
}
