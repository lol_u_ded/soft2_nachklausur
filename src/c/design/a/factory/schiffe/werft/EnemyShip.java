package c.design.a.factory.schiffe.werft;

public abstract class EnemyShip {

    private final String name;
    private double amtDamage;


    protected EnemyShip(String name, double amtDamage) {
        this.name = name;
        this.amtDamage = amtDamage;
    }

    public void followHeroShip(){
        System.out.println( name + " follows the HeroShip");
    }

    public void displayEnemyShip(){
        System.out.println( name + " appears at the horizon, ...");
    }

    public void enemyShipShoots(){
        System.out.println( name + " is shooting some cannon balls at your ship!");
    }

    public void setDamage (double newDamage){
        amtDamage = newDamage;
    }

    public double getDamage(){
        return amtDamage;
    }

    protected String getName(){
        return name;
    }
}
