package c.design.a.factory.schiffe.werft;

public class EnemyShipFactory {

    public final double MIN_DAMAGE_SMALL = 0;
    public final double MIN_DAMAGE_BIG = 100;
    public final double MAX_DAMAGE_SMALL = 50;
    public final double MAX_DAMAGE_BIG = 800;
    public final double DEFAULT_DAMAGE_BIG = 100;
    public final double DEFAULT_DAMAGE_SMALL = 25;
    public final String DEFAULT_NAME_SMALL = "Cruiser 11";
    public final String DEFAULT_NAME_BIG = "Standard Fregatte";

    public EnemyShip makeUFO(){
        return new UFO();
    }

    public EnemyShip makeFregatte(){
        return new Fregatte(DEFAULT_NAME_BIG, DEFAULT_DAMAGE_BIG);
    }

    public EnemyShip makeCustomBigShip(String name, double damage){
        if(damage < MIN_DAMAGE_BIG || damage > MAX_DAMAGE_BIG){
            return new Fregatte(name, DEFAULT_DAMAGE_BIG);
        }else {
            return new Fregatte(name, damage);
        }
    }

    public EnemyShip makeShip(){
        return new SmallShip(DEFAULT_NAME_SMALL, DEFAULT_DAMAGE_SMALL);
    }

    public EnemyShip makeCustomSmallShip(String name, double damage){
        if(damage > MAX_DAMAGE_SMALL|| damage < MIN_DAMAGE_SMALL){
            return new SmallShip(name, DEFAULT_DAMAGE_SMALL);
        }else{
            return new SmallShip(name, damage);
        }
    }
}

