package c.design.a.factory.schiffe.facdemo;

public class FacPattern {
    public static void main(String[] args) {

        Factory factory = new Factory();

        Product a = factory.getProductA("Fluxgenerator");
        Product b = factory.getProductB("1 Liter Flogiston");

        System.out.println(a.calculate());
        System.out.println(b.calculate());

    }
}

class Factory {

    Product getProductA(String name) {
        return new A(10, name);
    }

    Product getProductB(String name) {
        return new B(50, name);
    }

}

abstract class Product {

    final int integer;
    final String string;

    Product(int integer, String string) {
        this.string = string;
        this.integer = integer;
    }

    abstract int calculate();
}

class A extends Product {

    A(int rawPrice, String code) {
        super(rawPrice, code);
    }

    @Override
    int calculate() {
        return integer * string.length();
    }
}

class B extends Product {

    public B(int price, String name) {
        super(price, name);
    }

    @Override
    int calculate() {
        return integer + integer / 2;
    }
}
