package c.design.a.factory.schiffe;

import c.design.a.factory.schiffe.werft.EnemyShip;
import c.design.a.factory.schiffe.werft.EnemyShipFactory;

public class Main {
    public static void main(String[] args) {
        EnemyShipFactory werft = new EnemyShipFactory();

        EnemyShip f1 = werft.makeFregatte();
        EnemyShip f2 = werft.makeCustomBigShip("Aragon", 400);
        EnemyShip f3 = werft.makeShip();
        EnemyShip f4 = werft.makeCustomSmallShip(" Affi", 23);


        f1.displayEnemyShip();
        f2.displayEnemyShip();
        f2.followHeroShip();
        f1.enemyShipShoots();
        f3.displayEnemyShip();
        f3.followHeroShip();
        f3.enemyShipShoots();
        f2.enemyShipShoots();
        f1.enemyShipShoots();
        f2.enemyShipShoots();
        f4.displayEnemyShip();
        f4.enemyShipShoots();


    }
}
