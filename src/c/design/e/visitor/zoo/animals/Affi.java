package c.design.e.visitor.zoo.animals;

import c.design.e.visitor.zoo.spec.AnimalVisitor;

public class Affi implements AnimalZ {


    @Override
    public void accept(AnimalVisitor animalVisitor) {
        animalVisitor.visit(this);
    }
}
