package c.design.e.visitor.zoo.animals;

import c.design.e.visitor.zoo.spec.AnimalVisitor;

public interface AnimalZ {

    public void accept(AnimalVisitor animalVisitor);
}
