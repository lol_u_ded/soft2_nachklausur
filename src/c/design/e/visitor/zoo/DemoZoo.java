package c.design.e.visitor.zoo;

import c.design.e.visitor.zoo.animals.Affi;
import c.design.e.visitor.zoo.spec.Eat;
import c.design.e.visitor.zoo.spec.Unzufrieden;

public class DemoZoo {

    public static void main(String[] args) {

        Affi rudi = new Affi();
        rudi.accept(new Unzufrieden());
        rudi.accept(new Eat());
    }


}
