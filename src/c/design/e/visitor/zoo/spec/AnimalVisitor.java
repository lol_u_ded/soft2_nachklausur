package c.design.e.visitor.zoo.spec;

import c.design.e.visitor.zoo.animals.Affi;

public interface AnimalVisitor {

    void visit(Affi affi);
}
