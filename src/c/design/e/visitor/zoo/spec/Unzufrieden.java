package c.design.e.visitor.zoo.spec;

import c.design.e.visitor.zoo.animals.Affi;

public class Unzufrieden implements AnimalVisitor {
    @Override
    public void visit(Affi affi) {
        System.out.println("Affi will affin!  >:( <3 ");
    }
}
