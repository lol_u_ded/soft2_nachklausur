package c.design.e.visitor.zoo.spec;

import c.design.e.visitor.zoo.animals.Affi;

import java.sql.SQLOutput;

public class Eat implements AnimalVisitor {

    @Override
    public void visit(Affi affi) {
        System.out.println("Affi is eating <3");
    }
}
