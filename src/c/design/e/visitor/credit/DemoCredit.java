package c.design.e.visitor.credit;

import c.design.e.visitor.credit.creditcards.BronzeCreditCard;
import c.design.e.visitor.credit.creditcards.GoldCreditCard;
import c.design.e.visitor.credit.offers.GasOfferVisitor;
import c.design.e.visitor.credit.offers.HotelOfferVisitor;

public class DemoCredit {
    public static void main(String[] args) {

        // https://www.youtube.com/watch?v=TeZqKnC2gvA&list=PLmGDKWdXhyL2urJcgcfqR1xtcTXlGJMpH&index=37&t=0s

        HotelOfferVisitor hotelVisitor = new HotelOfferVisitor();
        GasOfferVisitor gasVisitor = new GasOfferVisitor();

        BronzeCreditCard bronze = new BronzeCreditCard();
        GoldCreditCard gold = new GoldCreditCard();

        bronze.accept(hotelVisitor);
        gold.accept(gasVisitor);

    }
}


