package c.design.e.visitor.credit.creditcards;


import c.design.e.visitor.credit.offers.OfferVisitor;

public interface CreditCards {
    String getName();

    void accept(OfferVisitor visitor);
}
