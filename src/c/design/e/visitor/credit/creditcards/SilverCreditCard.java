package c.design.e.visitor.credit.creditcards;


import c.design.e.visitor.credit.offers.OfferVisitor;

public class SilverCreditCard implements CreditCards {
    @Override
    public String getName() {
        return "Silver";
    }

    @Override
    public void accept(OfferVisitor visitor) {
        visitor.visitSilverCreditCard(this);
    }
}
