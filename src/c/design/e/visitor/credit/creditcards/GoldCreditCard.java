package c.design.e.visitor.credit.creditcards;


import c.design.e.visitor.credit.offers.OfferVisitor;

public class GoldCreditCard implements CreditCards {
    @Override
    public String getName() {
        return "Gold";
    }

    @Override
    public void accept(OfferVisitor visitor) {
        visitor.visitGoldCreditCard(this);
    }
}
