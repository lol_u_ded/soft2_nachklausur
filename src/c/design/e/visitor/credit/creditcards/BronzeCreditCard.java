package c.design.e.visitor.credit.creditcards;


import c.design.e.visitor.credit.offers.OfferVisitor;

public class BronzeCreditCard implements CreditCards {
    @Override
    public String getName() {
        return "Bronze";
    }

    @Override
    public void accept(OfferVisitor visitor) {
        visitor.visitBronzeCreditCard(this);
    }
}
