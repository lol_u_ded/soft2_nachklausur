package c.design.e.visitor.credit.offers;


import c.design.e.visitor.credit.creditcards.BronzeCreditCard;
import c.design.e.visitor.credit.creditcards.GoldCreditCard;
import c.design.e.visitor.credit.creditcards.SilverCreditCard;

public interface OfferVisitor {

    void visitBronzeCreditCard(BronzeCreditCard bronze);

    void visitSilverCreditCard(SilverCreditCard silver);

    void visitGoldCreditCard(GoldCreditCard gold);
}
