package c.design.e.visitor.credit.offers;


import c.design.e.visitor.credit.creditcards.BronzeCreditCard;
import c.design.e.visitor.credit.creditcards.GoldCreditCard;
import c.design.e.visitor.credit.creditcards.SilverCreditCard;

public class GasOfferVisitor implements OfferVisitor {

    @Override
    public void visitBronzeCreditCard(BronzeCreditCard bronze) {
        System.out.println(" Computing gas offer for " + bronze.getName() + "CreditCard ...");
    }

    @Override
    public void visitSilverCreditCard(SilverCreditCard silver) {
        System.out.println(" Computing gas offer for " + silver.getName() + "CreditCard ...");
    }

    @Override
    public void visitGoldCreditCard(GoldCreditCard gold) {
        System.out.println(" Computing gas offer for " + gold.getName() + "CreditCard ...");
    }
}
