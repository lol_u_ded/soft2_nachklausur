package c.design.e.visitor.dürüm.pers;

import c.design.e.visitor.dürüm.best.Dürüm;
import c.design.e.visitor.dürüm.best.Kebab;
import c.design.e.visitor.dürüm.best.Muslym;

public class Ibrahim implements Visitor {

    private String begrüßung = "Hey Ibrahim, Walla was Los? ";

    @Override
    public void visitDürüm(Dürüm dürüm) {
        System.out.println(begrüßung + "hier nimm Dürüm Bruder! " + dürüm.getPrice() + "€");
    }

    @Override
    public void visitKebab(Kebab kebab) {
        System.out.println(begrüßung + "hier nimm Kebab Bruder! " + kebab.getPrice() + "€");
    }

    @Override
    public void visitMuslym(Muslym muslym) {
        System.out.println(begrüßung + "hier nimm Muslym Bruder! " + muslym.getPrice() + "€");
    }
}
