package c.design.e.visitor.dürüm.pers;

import c.design.e.visitor.dürüm.best.Dürüm;
import c.design.e.visitor.dürüm.best.Kebab;
import c.design.e.visitor.dürüm.best.Muslym;

public interface Visitor {

    void visitDürüm(Dürüm dürüm);

    void visitKebab(Kebab kebab);

    void visitMuslym(Muslym muslym);
}
