package c.design.e.visitor.dürüm.pers;

import c.design.e.visitor.dürüm.best.Dürüm;
import c.design.e.visitor.dürüm.best.Kebab;
import c.design.e.visitor.dürüm.best.Muslym;

public class Police implements Visitor {

    private String begrüßung = "Was los? ";

    @Override
    public void visitDürüm(Dürüm dürüm) {
        System.out.println(begrüßung + "Hier Dürüm " + dürüm.getPrice() * 1.50 + "€");
    }

    @Override
    public void visitKebab(Kebab kebab) {
        System.out.println(begrüßung + "Hier Kebab " + kebab.getPrice() * 1.65 + "€");
    }

    @Override
    public void visitMuslym(Muslym muslym) {
        System.out.println(begrüßung + "Hier Muslym " + muslym.getPrice() * 2.0 + "€");
    }

}
