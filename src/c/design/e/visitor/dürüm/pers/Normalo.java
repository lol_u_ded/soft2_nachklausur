package c.design.e.visitor.dürüm.pers;

import c.design.e.visitor.dürüm.best.Dürüm;
import c.design.e.visitor.dürüm.best.Kebab;
import c.design.e.visitor.dürüm.best.Muslym;

public class Normalo implements Visitor {

    private String begrüßung = "Hallo. ";

    @Override
    public void visitDürüm(Dürüm dürüm) {
        System.out.println(begrüßung + "Ein Dürüm, macht " + dürüm.getPrice() * 1.25 + "€");
    }

    @Override
    public void visitKebab(Kebab kebab) {
        System.out.println(begrüßung + "Ein Kebab, macht " + kebab.getPrice() * 1.25 + "€");
    }

    @Override
    public void visitMuslym(Muslym muslym) {
        System.out.println(begrüßung + "Ein Muslym, macht " + muslym.getPrice() * 1.25 + "€");
    }

}
