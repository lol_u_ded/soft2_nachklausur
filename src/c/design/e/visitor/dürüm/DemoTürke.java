package c.design.e.visitor.dürüm;

import c.design.e.visitor.dürüm.best.Dürüm;
import c.design.e.visitor.dürüm.best.Kebab;
import c.design.e.visitor.dürüm.best.Muslym;
import c.design.e.visitor.dürüm.pers.Ibrahim;
import c.design.e.visitor.dürüm.pers.Normalo;
import c.design.e.visitor.dürüm.pers.Police;

public class DemoTürke {
    public static void main(String[] args) {
        Dürüm dürüm = new Dürüm();
        Kebab kebab = new Kebab();
        Muslym muslym = new Muslym();

        Ibrahim walla = new Ibrahim();
        Normalo kuffa = new Normalo();
        Police hurensohn = new Police();

        System.out.println("Dürüm Bestellung");
        dürüm.accept(walla);
        dürüm.accept(kuffa);
        dürüm.accept(hurensohn);

        System.out.println("\nKebab Bestellung");
        kebab.accept(walla);
        kebab.accept(kuffa);
        kebab.accept(hurensohn);

        System.out.println("\nMuslym Bestellung");
        muslym.accept(walla);
        muslym.accept(kuffa);
        muslym.accept(hurensohn);

    }
}
