package c.design.e.visitor.dürüm.best;

import c.design.e.visitor.dürüm.pers.Visitor;

public class Kebab implements Food {


    @Override
    public double getPrice() {
        return 4.2;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visitKebab(this);
    }
}

