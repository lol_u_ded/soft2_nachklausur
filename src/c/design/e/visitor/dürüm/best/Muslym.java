package c.design.e.visitor.dürüm.best;

import c.design.e.visitor.dürüm.pers.Visitor;

public class Muslym implements Food {

    @Override
    public double getPrice() {
        return 2.3;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visitMuslym(this);
    }
}
