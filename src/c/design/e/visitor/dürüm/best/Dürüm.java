package c.design.e.visitor.dürüm.best;

import c.design.e.visitor.dürüm.pers.Visitor;

public class Dürüm implements Food {


    @Override
    public double getPrice() {
        return 3.2;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visitDürüm(this);
    }
}
