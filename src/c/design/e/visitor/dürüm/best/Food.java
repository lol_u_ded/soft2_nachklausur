package c.design.e.visitor.dürüm.best;

import c.design.e.visitor.dürüm.pers.Visitor;

public interface Food {

    double getPrice();

    void accept(Visitor visitor);
}
