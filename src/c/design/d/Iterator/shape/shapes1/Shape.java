package c.design.d.Iterator.shape.shapes1;

public class Shape {

    private final int id;
    private final String name;

    public Shape(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Shape{" + name + ",id= " + id + '\'' + '}';
    }
}

