package c.design.d.Iterator.shape.shapes1;

import java.util.Iterator;

public class ShapeIterator implements Iterator<Shape> {

    private final Shape[] shapes;
    private int index = 0;

    public ShapeIterator(Shape[] shapes) {
        this.shapes = shapes;
    }

    @Override
    public boolean hasNext() {
        if (index < shapes.length && shapes[index] != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Shape next() {
        Shape ret = shapes[index];
        index++;
        return ret;
    }
}
