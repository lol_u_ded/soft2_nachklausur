package c.design.d.Iterator.shape.shapes1;

public class ShapeContainer {

    private final Shape[] shapes;
    private final int size;
    private int idx = 0;

    public ShapeContainer(int size) {
        this.shapes = new Shape[size];
        this.size = size;
    }

    public boolean add(Shape shape) {
        if (!isFull()) {
            shapes[idx] = shape;
            idx++;
            return true;
        } else {
            return false;
        }
    }

    public Shape[] getShapes() {
        return shapes;
    }

    public int getSize() {
        return size;
    }

    public boolean isFull() {
        return idx >= shapes.length;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Container: ");
        for (Shape shape : shapes) {
            sb.append(" ").append(shape).append("\n");
        }

        return sb.toString();
    }
}
