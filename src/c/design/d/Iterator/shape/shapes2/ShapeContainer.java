package c.design.d.Iterator.shape.shapes2;

import java.util.Iterator;

public class ShapeContainer {

    private final Shape[] shapes;
    private final int size;
    private int idx = 0;

    public ShapeContainer(int size) {
        this.shapes = new Shape[size];
        this.size = size;
    }

    public boolean add(Shape shape) {
        if (!isFull()) {
            shapes[idx] = shape;
            idx++;
            return true;
        } else {
            return false;
        }
    }

    public Shape[] getShapes() {
        return shapes;
    }

    public int getSize() {
        return size;
    }

    public boolean isFull() {
        return idx >= shapes.length;
    }

    public Iterator<Shape> getIterator() {
        return new Iterator<Shape>() {

            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < shapes.length && shapes[index] != null;
            }

            @Override
            public Shape next() {
                Shape ret = shapes[index];
                index++;
                return ret;
            }
        };

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Container: ");
        for (Shape shape : shapes) {
            sb.append(" ").append(shape).append("\n");
        }

        return sb.toString();
    }
}
