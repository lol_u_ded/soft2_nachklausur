package c.design.d.Iterator;

import c.design.d.Iterator.shape.shapes1.Shape;
import c.design.d.Iterator.shape.shapes1.ShapeContainer;
import c.design.d.Iterator.shape.shapes1.ShapeIterator;

import java.util.Iterator;

public class DemoShapes {

    public static void main(String[] args) {
        ShapeContainer container = new ShapeContainer(4);

        container.add(new Shape(1, "Rectangle"));
        container.add(new Shape(2, "Triangle"));
        container.add(new Shape(3, "Circle"));
        container.add(new Shape(4, "Star"));
        System.out.println(" Expected true --> " + container.isFull());
        System.out.println(container);

        Iterator<Shape> shapeIterator = new ShapeIterator(container.getShapes());

        System.out.println("ShapeIterator");
        while (shapeIterator.hasNext()) {
            System.out.println(shapeIterator.next());
        }
    }
}
