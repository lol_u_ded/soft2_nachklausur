package c.design.d.Iterator;


import c.design.d.Iterator.shape.shapes2.Shape;
import c.design.d.Iterator.shape.shapes2.ShapeContainer;

import java.util.Iterator;

public class DemoShapes2 {

    public static void main(String[] args) {
        ShapeContainer container = new ShapeContainer(6);

        container.add(new Shape(1, "Rectangle"));
        container.add(new Shape(2, "Triangle"));
        container.add(new Shape(3, "Circle"));
        container.add(new Shape(4, "Star"));
        container.add(new Shape(5, "Octaeder"));
        System.out.println(" Expected false --> " + container.isFull() + "\n");
        System.out.println(container);

        Iterator<Shape> shapeIterator = container.getIterator();

        System.out.println("ShapeIterator inner class");
        while (shapeIterator.hasNext()) {
            System.out.println(shapeIterator.next());
        }
    }
}
