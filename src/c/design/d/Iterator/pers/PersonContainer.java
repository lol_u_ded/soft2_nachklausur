package c.design.d.Iterator.pers;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class PersonContainer implements Iterable<Person> {

    private final Person[] container;
    private final int size;
    private int counter = 0;

    public PersonContainer(int size) {
        this.size = size;
        this.container = new Person[size];

    }

    @Override
    public Iterator<Person> iterator() {

        return new Iterator<Person>() {

            int at = 0;


            @Override
            public boolean hasNext() {
                if (at >= size) {
                    return false;
                } else {
                    return container[at] != null;
                }
            }

            @Override
            public Person next() {
                if (hasNext()) {
                    Person ret = container[at];
                    at++;
                    return ret;
                } else {
                    throw new NoSuchElementException();
                }
            }
        };
    }

    public void addPerson(Person p) {
        if (counter < size) {
            container[counter] = p;
            counter++;
        } else {
            System.out.println("Ich bin voll");
        }
    }

    public Person[] getContainer() {
        return container;
    }

    public int getSize() {
        return size;
    }


}
