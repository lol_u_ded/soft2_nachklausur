package c.design.d.Iterator;

import c.design.d.Iterator.pers.Person;
import c.design.d.Iterator.pers.PersonContainer;

import java.util.Iterator;

public class DemoPersItr {
    public static void main(String[] args) {

        PersonContainer cont = new PersonContainer(5);

        cont.addPerson(new Person("Max", "Mustermann", 20));
        cont.addPerson(new Person("Aaron", "Lee", 30));
        cont.addPerson(new Person("Eckard", "Meier", 40));
        cont.addPerson(new Person("Michael", "Schmidt", 25));
        cont.addPerson(new Person("Herbert", "Winchester", 35));


        // Lambdas
//         cont.forEach(x -> System.out.println(x));


        //For schleife
//        for(Person p : cont){
//            System.out.println(p);
//        }


        // Iterator

        Iterator<Person> itr = cont.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }
}
