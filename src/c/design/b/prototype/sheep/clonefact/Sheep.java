package c.design.b.prototype.sheep.clonefact;

public class Sheep implements Animal {

    String name;

    public Sheep(String name) {
        this.name = name;
        System.out.println(name + " is made");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Animal makeCopy() {
        try {
            System.out.println("Cloning Sheep " + name);
            return (Sheep) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }


}
