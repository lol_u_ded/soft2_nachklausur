package c.design.b.prototype.sheep.clonefact;

public class PrototypeFactory {

    public Animal getClone(Animal animal) {
        return animal.makeCopy();
    }
}
