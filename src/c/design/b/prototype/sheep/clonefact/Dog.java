package c.design.b.prototype.sheep.clonefact;

public class Dog implements Animal {

    String name;

    public Dog(String name) {
        this.name = name;
        System.out.println(name + " is made");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Animal makeCopy() {
        try {
            System.out.println("Cloning Dog " + name);
            return (Dog) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
