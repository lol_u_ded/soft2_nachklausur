package c.design.b.prototype.sheep.clonefact;

public interface Animal extends Cloneable {

    Animal makeCopy();

}
