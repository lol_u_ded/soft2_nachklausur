package c.design.b.prototype.sheep;

import c.design.b.prototype.sheep.clonefact.PrototypeFactory;
import c.design.b.prototype.sheep.clonefact.Sheep;

public class TestingDolly {

    public static void main(String[] args) {


        Sheep sheep = new Sheep("Dolly");

        PrototypeFactory factory = new PrototypeFactory();

        Sheep clonedSheep = null;
        clonedSheep = (Sheep) factory.getClone(sheep);

        System.out.println("Clone: " + clonedSheep.getName());
        System.out.println("OG   : " + sheep.getName());
        clonedSheep.setName("Olga");
        System.out.println(" renamed cloned Sheep to Olga");

        System.out.println("Clone: " + clonedSheep.getName());
        System.out.println("OG   : " + sheep.getName());

        sheep.setName("Jusuf");
        System.out.println(" renamed sheep to Jusuf");

        System.out.println("OG   : " + clonedSheep.getName());
        System.out.println("OG   : " + sheep.getName());
    }

}
