package c.design.b.prototype.tool;

import java.awt.*;

public class Rectangle extends Figure {

    public Rectangle(Color color, String shape) {
        super(color, shape);
    }

    public void draw() {
        System.out.println("Drawing " + this.getShape() + " Line & it's Value is [" + this.getColor() + "]. ");
    }

    @Override
    public Figure makeCopy() {
        try {
            System.out.println("Cloning Rectangle");
            return (Rectangle) this.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
