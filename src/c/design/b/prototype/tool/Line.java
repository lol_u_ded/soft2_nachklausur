package c.design.b.prototype.tool;

import java.awt.*;

public class Line extends Figure {

    public Line(Color color, String shape) {
        super(color, shape);
    }

    public void draw() {
        System.out.println("Drawing " + this.getShape() + " Line & it's value is [" + this.getColor() + "]. ");
    }

    @Override
    public Figure makeCopy() {
        try {
            System.out.println("Cloning Line");
            return (Line) this.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
