package c.design.b.prototype.tool;

import java.awt.*;
import java.util.List;

public class Client {
    public static void main(String[] args) {
        List<Figure> modules = List.of(new Rectangle(Color.BLACK, "DefaultSize"), new Line(Color.BLACK, "DefaultThickness"));

        Figure f1 = modules.get(1).makeCopy();
        Figure f2 = modules.get(1).makeCopy();

        f1.setColor(Color.RED);
        f2.setShape("BigSize");

        f1.draw();
        f2.draw();


    }
}
