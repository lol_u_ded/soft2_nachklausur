package c.design.b.prototype.tool;

import java.awt.*;

public abstract class Figure implements Cloneable {

    private Color color;
    private String shape;

    public Figure(Color color, String shape) {
        this.color = color;
        this.shape = shape;
    }

    abstract void draw();

    public abstract Figure makeCopy();

    // getter & setter
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }
}
