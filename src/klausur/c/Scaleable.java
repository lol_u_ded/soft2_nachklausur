package klausur.c;

public interface Scaleable {

    void scale(double factor);

    default void doubled() {
        scale(2);
    }
}
