package klausur;

import klausur.b.ComposedMessage;
import klausur.b.Message;
import klausur.b.SingleLine;

public class DemoB {

    public static void main(String[] args) {

        Message msgFromDonald =
                new ComposedMessage(
                        new SingleLine("Message from the gratest on earth!"),
                        new ComposedMessage(
                                new SingleLine("I am the greatest!"),
                                new SingleLine("And all the others are losers."),
                                new SingleLine("That's true!")
                        ),
                        new SingleLine("Your Donald")
                );

        msgFromDonald.forEach( n -> System.out.println(n));
//        msgFromDonald.forEach(System.out::println);
    }
}
