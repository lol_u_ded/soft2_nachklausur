package klausur.e;

import java.util.Objects;
import java.util.SortedSet;

public class Course implements Comparable<Course> {

    public final int year;
    public final String name;

    public Course(int year, String name) {
        this.year = year;
        this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, year);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Course) {
            Course other = (Course) obj;
            return this.name.equals(other.name) && this.year == other.year;
        }
        return false;
    }

    //Austeigend
    @Override
    public int compareTo(Course other) {
        if (other.name.equals(this.name)) {
            return Integer.compare(this.year, other.year);
        }
        return this.name.compareTo(other.name);
    }

//    // Absteigend
//    @Override
//    public int compareTo(Course other) {
//        if (other.name.equals(this.name)) {
//            return Integer.compare(other.year, this.year);
//        }
//        return other.name.compareTo(this.name) ;
//    }

    @Override
    public String toString() {
        return "Course{" +
                "year=" + year +
                ", name='" + name + '\'' +
                '}';
    }
}
