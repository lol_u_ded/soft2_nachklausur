package klausur.d;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Streams {
    public static void run() {

        List<String> words = List.of("This","list","contains","words","Ann","and","Pat");

        List<String> upperCaseWords = words
                .stream()
                .filter(n -> Character.isUpperCase(n.charAt(0)))
                .collect(Collectors.toList());

        System.out.print("UpperCase words \n [");
        for(String s : upperCaseWords){
            System.out.print(" " + s);
        }
        System.out.println(" ]\n");


        Comparator<String> byLength = (s1, s2) -> s1.length() - s2.length();
//        List<String> sorted = words.stream().sorted(byLength).collect(Collectors.toList());
//        sorted.forEach(System.out::println);
        Optional<String> shortestWord = words.stream().sorted(byLength).findFirst();
        Optional<String> shortestWord2 = words.stream().min(byLength);
        Optional<String> shortestWord3 = words.stream().sorted((x, y) -> Integer.compare(x.length(), y.length())).findFirst();
        System.out.println(shortestWord);
        System.out.println(shortestWord2);
        System.out.println(shortestWord3);
    }
}
