package klausur.a;

import java.util.List;

public abstract class Message {
    public abstract List<String> getLines();
}
