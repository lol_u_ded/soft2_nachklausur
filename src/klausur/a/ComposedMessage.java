package klausur.a;

import java.util.List;
import java.util.stream.Collectors;

public class ComposedMessage extends Message {

    public final List<Message> elems;           // List<List<String>>

    public ComposedMessage(Message... elems) {
        this.elems = List.of(elems);
    }

    @Override
    public List<String> getLines() {
        return elems
                .stream()
                // Message-Message-Message-Message-Message-Message-
                .map(elems -> elems.getLines())
                // List<Strings>-List<Strings>-List<Strings>-List<Strings>-List<Strings>-
                .flatMap(n -> n.stream())
                // String-String-String-...
                .collect(Collectors.toList());
    }
}
