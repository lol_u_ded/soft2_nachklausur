package klausur.a;

import java.util.List;

public class SingleLine extends Message {

    public final String line;

    public SingleLine(String line) {
        this.line = line;
    }


    @Override
    public List<String> getLines() {
        return List.of(line);
    }
}
