package klausur;

import klausur.f.ComposedMessage;
import klausur.f.CountContainsVisitor;
import klausur.f.Message;
import klausur.f.SingleLine;

public class DemoF {
    public static void main(String[] args) {
        Message msgfromDonald =
                new ComposedMessage(new SingleLine("Message from the greateston earth!"),
                        new ComposedMessage(
                                new SingleLine("I am the greatest!"),
                                new SingleLine("And all others are losers."),
                                new SingleLine("Thats the truth!")
                        ),
                        new SingleLine("Your great Donald")
                );
        CountContainsVisitor ccV = new CountContainsVisitor("great");
        ccV.visit(msgfromDonald);
        ccV.printCount();
    }


}
