package klausur;

import klausur.e.Course;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class DemoE {

    public static void main(String[] args) {

        SortedSet<Course> courseSorted = new TreeSet<>();
        courseSorted.add(new Course(1990, "Programmieren"));
        courseSorted.add(new Course(2000, "Programmieren"));
        courseSorted.add(new Course(2010, "Programmieren"));
        courseSorted.add(new Course(2020, "Programmieren"));


        SortedSet<Course> courseSortedByYear = new TreeSet<Course>(courseSorted
                .stream()
                .sorted((x, y) -> x.compareTo(y))
                .collect(Collectors.toSet())
        );

        courseSortedByYear.forEach(x -> System.out.println(x));
    }
}
