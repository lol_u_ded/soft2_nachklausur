package klausur.f;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ComposedMessage extends Message {

    private final List<String> elems;

    public ComposedMessage(Message... messages) {
        elems = new ArrayList<>();
        for (Message s : messages) {
            if (s != null) {
                elems.addAll(s.getElems());
            }
        }
    }

    @Override
    public List<String> getElems() {
        return elems;
    }

    @Override
    public void forEach(Consumer<String> consumer) {
        for (String s : elems) {
            consumer.accept(s);
        }
    }

    @Override
    public void accept(MessageVisitor visitor) {
        visitor.visit(this);
    }
}
