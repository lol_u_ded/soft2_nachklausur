package klausur.f;

import java.util.List;
import java.util.function.Consumer;

public class SingleLine extends Message {

    private final String line;

    public SingleLine(String line) {
        this.line = line;
    }

    @Override
    public List<String> getElems() {
        return List.of(line);
    }

    @Override
    public void forEach(Consumer<String> consumer) {
        consumer.accept(line);
    }

    @Override
    public void accept(MessageVisitor visitor) {
        visitor.visit(this);
    }
}
