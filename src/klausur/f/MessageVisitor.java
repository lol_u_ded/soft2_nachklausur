package klausur.f;


public interface MessageVisitor {

    void visit(Message message);

}
