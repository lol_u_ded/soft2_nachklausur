package klausur.f;

import java.util.List;
import java.util.function.Consumer;

public abstract class Message {

    public abstract List<String> getElems();

    public abstract void forEach(Consumer<String> consumer);

    public abstract void accept(MessageVisitor visitor);
}
