package klausur.f;

public class CountContainsVisitor implements MessageVisitor {

    private final String text;
    public int count = 0;

    public CountContainsVisitor(String text) {
        this.text = text;
    }

    @Override
    public void visit(Message message) {
        message.getElems()
                .stream()
                .filter(s -> s.contains(text))
                .forEach(t -> count++);
    }

    public void printCount() {
        System.out.println(count);
    }
}
