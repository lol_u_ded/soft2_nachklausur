package klausur.b;

import java.util.List;
import java.util.function.Consumer;

public abstract class Message {
    public abstract List<String> getLines();
    public abstract void forEach(Consumer<String> action);
}
