package klausur.b;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ComposedMessage extends Message {

    public final List<Message> elems;

    public ComposedMessage(Message... elems) {
        this.elems = List.of(elems);
    }

    @Override
    public List<String> getLines() {
        return elems.stream().map(n -> n.getLines()).flatMap(List::stream).collect(Collectors.toList());
    }

    @Override
    public void forEach(Consumer<String> action) {
        this.getLines().forEach(t -> action.accept(t));
    }
}
