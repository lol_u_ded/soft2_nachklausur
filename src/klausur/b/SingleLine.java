package klausur.b;

import java.util.List;
import java.util.function.Consumer;

public class SingleLine extends Message {

    public final String line;

    public SingleLine(String line) {
        this.line = line;
    }


    @Override
    public List<String> getLines() {
        return List.of(line);
    }

    @Override
    public void forEach(Consumer<String> action) {
        action.accept(line);
    }
}
