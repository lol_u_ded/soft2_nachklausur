package klausur;

import klausur.c.Rectangle;

public class DemoC {

    public static void main(String[] args) {

        Rectangle rectangle = new Rectangle(2, 2);
        System.out.println(rectangle);
        rectangle.doubled();
        System.out.println(rectangle);
        rectangle.scale(1.5);
        System.out.println(rectangle);
    }
}
