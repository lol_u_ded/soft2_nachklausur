package klausur;

import klausur.a.ComposedMessage;
import klausur.a.Message;
import klausur.a.SingleLine;

public class DemoA {

    public static void main(String[] args) {

        Message msgFromDonald =
                new ComposedMessage(
                        new SingleLine("Message from the gratest on earth!"),
                        new ComposedMessage(
                                new SingleLine("I am the greatest!"),
                                new SingleLine("And all the others are losers."),
                                new SingleLine("That's true!")
                        ),
                        new SingleLine("Your Donald")
                );

        for (String line : msgFromDonald.getLines()) {
            System.out.println(line);
        }
    }
}
